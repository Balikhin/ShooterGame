// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "Weapons/ShooterBaseThrow.h"
#include "ShooterGame.h"
#include "Particles/ParticleSystemComponent.h"
#include "Effects/ShooterExplosionEffect.h"

AShooterBaseThrow::AShooterBaseThrow(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	CollisionComp = ObjectInitializer.CreateDefaultSubobject<USphereComponent>(this, TEXT("SphereComp"));
	CollisionComp->InitSphereRadius(5.0f);
	CollisionComp->AlwaysLoadOnClient = true;
	CollisionComp->AlwaysLoadOnServer = true;
	CollisionComp->bTraceComplexOnMove = true;
	CollisionComp->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	CollisionComp->SetCollisionObjectType(COLLISION_PROJECTILE);
	CollisionComp->SetCollisionResponseToAllChannels(ECR_Ignore);
	CollisionComp->SetCollisionResponseToChannel(ECC_WorldStatic, ECR_Block);
	CollisionComp->SetCollisionResponseToChannel(ECC_WorldDynamic, ECR_Block);
	CollisionComp->SetCollisionResponseToChannel(ECC_Pawn, ECR_Block);
	RootComponent = CollisionComp;

	ParticleComp = ObjectInitializer.CreateDefaultSubobject<UParticleSystemComponent>(this, TEXT("ParticleComp"));
	ParticleComp->bAutoActivate = false;
	ParticleComp->bAutoDestroy = false;
	ParticleComp->SetupAttachment(RootComponent);

	MovementComp = ObjectInitializer.CreateDefaultSubobject<UProjectileMovementComponent>(this, TEXT("ProjectileComp"));
	MovementComp->UpdatedComponent = CollisionComp;
	MovementComp->InitialSpeed = 1000.0f;
	MovementComp->MaxSpeed = 1000.0f;
	MovementComp->bRotationFollowsVelocity = true;
	MovementComp->ProjectileGravityScale = 1.0f;

	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.TickGroup = TG_PrePhysics;
	SetRemoteRoleForBackwardsCompat(ROLE_SimulatedProxy);
	bReplicates = true;
	bReplicateMovement = true;
}

void AShooterBaseThrow::PostInitializeComponents()
{
	Super::PostInitializeComponents();
	MovementComp->OnProjectileStop.AddDynamic(this, &AShooterBaseThrow::OnImpact);
	CollisionComp->MoveIgnoreActors.Add(Instigator);

	AShooterThrow_Projectile* OwnerWeapon = Cast<AShooterThrow_Projectile>(GetOwner());
	if (OwnerWeapon)
	{
		OwnerWeapon->ApplyWeaponConfig(WeaponConfig);
	}

	SetLifeSpan( WeaponConfig.ProjectileLife );
	MyController = GetInstigatorController();
}

void AShooterBaseThrow::InitVelocity(FVector& ShootDirection)
{
	if (MovementComp)
	{
		MovementComp->Velocity = ShootDirection * MovementComp->InitialSpeed;
	}
}

void AShooterBaseThrow::OnImpact(const FHitResult& HitRes)
{
	if (Role == ROLE_Authority && !bImpacted)
	{
		HitResult = HitRes;
		Impact();
		//DisableAndDestroy();
	}
}

void AShooterBaseThrow::Impact()
{
	if (ParticleComp)
	{
		ParticleComp->Deactivate();
	}

	//WeaponConfig::ImpactDelay if >0 Delayed Impact
	FTimerHandle UnusedHandle;
	GetWorldTimerManager().SetTimer(UnusedHandle, this, &AShooterBaseThrow::MakeImpact, WeaponConfig.ImpactDelay, false);

	bImpacted = true;
}

void AShooterBaseThrow::MakeImpact()
{
	if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 4.0f, FColor::Red, TEXT("Make Explosion"));
	// effects and damage origin shouldn't be placed inside mesh at impact point
	const FVector NudgedImpactLocation = HitResult.ImpactPoint + HitResult.ImpactNormal * 10.0f;

	if (WeaponConfig.ImpactDamage > 0 && WeaponConfig.ImpactRadius > 0 && WeaponConfig.DamageType)
	{
		UGameplayStatics::ApplyRadialDamage(this, WeaponConfig.ImpactDamage, NudgedImpactLocation, WeaponConfig.ImpactRadius, WeaponConfig.DamageType, TArray<AActor*>(), this, MyController.Get());
	}

	if (ImpactEffectTemplate)
	{
		FTransform const SpawnTransform(HitResult.ImpactNormal.Rotation(), NudgedImpactLocation);
		AShooterExplosionEffect* const EffectActor = GetWorld()->SpawnActorDeferred<AShooterExplosionEffect>(ImpactEffectTemplate, SpawnTransform);
		if (EffectActor)
		{
			EffectActor->SurfaceHit = HitResult;
			UGameplayStatics::FinishSpawningActor(EffectActor, SpawnTransform);
		}
	}

	DisableAndDestroy();
}

void AShooterBaseThrow::DisableAndDestroy()
{
	UAudioComponent* ProjAudioComp = FindComponentByClass<UAudioComponent>();
	if (ProjAudioComp && ProjAudioComp->IsPlaying())
	{
		ProjAudioComp->FadeOut(0.1f, 0.f);
	}

	MovementComp->StopMovementImmediately();

	// give clients some time to show explosion
	SetLifeSpan( 2.0f );
}

///CODE_SNIPPET_START: AActor::GetActorLocation AActor::GetActorRotation
void AShooterBaseThrow::OnRep_Impacted()
{
	FVector ProjDirection = GetActorForwardVector();

	const FVector StartTrace = GetActorLocation() - ProjDirection * 200;
	const FVector EndTrace = GetActorLocation() + ProjDirection * 150;
	FHitResult HitRes;
	
	if (!GetWorld()->LineTraceSingleByChannel(HitRes, StartTrace, EndTrace, COLLISION_PROJECTILE, FCollisionQueryParams(SCENE_QUERY_STAT(ProjClient), true, Instigator)))
	{
		// failsafe
		HitRes.ImpactPoint = GetActorLocation();
		HitRes.ImpactNormal = -ProjDirection;
	}
	HitResult = HitRes;
	Impact();
}
///CODE_SNIPPET_END

void AShooterBaseThrow::PostNetReceiveVelocity(const FVector& NewVelocity)
{
	if (MovementComp)
	{
		MovementComp->Velocity = NewVelocity;
	}
}

void AShooterBaseThrow::GetLifetimeReplicatedProps( TArray< FLifetimeProperty > & OutLifetimeProps ) const
{
	Super::GetLifetimeReplicatedProps( OutLifetimeProps );
	
	DOREPLIFETIME( AShooterBaseThrow, bImpacted );
}